import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.css']
})
export class PlaygroundComponent implements OnInit {
  squares: string[];
  nextPlayer: string;
  winner: string;

  constructor() { }

  ngOnInit() {
    this.newGame();
    
  }

  newGame(){
    this.squares=Array(9).fill(null);
    this.nextPlayer='O';

  }

  get player(){
    if(this.nextPlayer=='X'){
      this.nextPlayer='O';
      return 'O';
      
    }
    else{
      this.nextPlayer='X';
      return 'X';
      
    }
    
  }
  

  changeState(i:number){

    if(!this.squares[i] && !this.winner){
      this.squares.splice(i, 1, this.player);

    }
    

    this.winner=this.checkWinner();

   

  }
  checkWinner(){

    const lines = [
          [0, 1, 2],
          [3, 4, 5],
          [6, 7, 8],
          [0, 3, 6],
          [1, 4, 7],
          [2, 5, 8],
          [0, 4, 8],
          [2, 4, 6]
        ];

        for (let i = 0; i < lines.length; i++) {
              const [a, b, c] = lines[i];
              if (
                this.squares[a] &&
                this.squares[a] === this.squares[b] &&
                this.squares[a] === this.squares[c]
              ) {
                return this.squares[a];
              }
            }
            return null;
          }

  

}
